#include <iostream>
#include "queue.h"


void initQueue(queue* q, unsigned size)
{
	int i = 0;
	q->_maxSize = size;
	q->_count = 0;

	q->_elements = new int[q->_maxSize];

	for (i = 0; i < q->_maxSize; i++)
	{
		q->_elements[i] = -1;
	}
}


void cleanQueue(queue* q)
{
	int i = 0;

	delete[] q->_elements;
	q->_elements = new int[q->_maxSize];
	
	for (i = 0; i < q->_maxSize; i++)
	{
		q->_elements[i] = -1;
	}
}


void enqueue(queue* q, unsigned newValue)
{
	int i = getLastIndex(q, false);

	if (newValue < 0 || i == -1 || i >= q->_maxSize)
	{
		return;
	}

	q->_elements[i] = newValue;
}


int dequeue(queue* q)
{
	int value = -1;
	int i = getLastIndex(q, true);

	if (i != -1)
	{
		value = q->_elements[i];
		q->_elements[i] = -1;
	}

	return value;
}


int getLastIndex(queue* q, bool add)
{
	int i = 0;

	for (int i = 0; i < q->_maxSize; i++)
	{
		if (q->_elements[i] == -1 || (add && i + 1 == q->_maxSize))
		{
			return i;
		}
	}

	return -1;
}


void printQueue(queue* q)
{
	int i = 0;

	for (int i = 0; i < q->_maxSize && q->_elements[i] >= 0; i++)
	{
		std::cout << i + 1 << ". " << q->_elements[i] << std::endl;
	}
}
