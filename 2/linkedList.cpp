#include <iostream>
#include "linkedList.h"


void initLinkedList(linkedList* head, unsigned data)
{
	head->next = nullptr;
	head->data = data;
}


void cleanLinkedList(linkedList* head)
{
	while (head->next)
	{
		head->next = removeFromLinkedList(head->next, 0);
	}
	head->data = NULL;
}


void addToLinkedList(linkedList* head, unsigned num)
{
	linkedList* last = getLastLinkedList(head);

	linkedList* obj = new linkedList;
	initLinkedList(obj, num);
	
	last->next = obj;
}


linkedList* removeFromLinkedList(linkedList* head, int i)
{
	int size = getLinkedListSize(head);

	if (i < 0 || i > size) return head;

	linkedList* prev = 0;
	linkedList* curr = head;

	while (curr->next && i)
	{
		prev = curr;
		curr = curr->next;
		i--;
	}

	if (prev)
	{
		prev->next = curr->next;
		delete(curr);
		return head;
	}

	head = curr->next;
	delete(curr);
	return head;
}


linkedList* getLastLinkedList(linkedList* head)
{
	linkedList* curr = head;
	while (curr->next)
	{
		curr = curr->next;
	}
	return curr;
}


void printLinkedList(linkedList* head)
{
	if (!head || !head->data) return;

	linkedList* curr = head;
	int i = 0;
	
	while (curr && curr->data >= 0)
	{
		std::cout << i + 1 << ". " << curr->data << std::endl;
		curr = curr->next;
		i++;
	}
}

int getLinkedListSize(linkedList* head)
{
	int i = 0;
	while (head)
	{
		head = head->next;
		i++;
	}
	return i;
}
