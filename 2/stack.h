#ifndef STACK_H
#define STACK_H

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	unsigned* _elements;
	unsigned _count;
} stack;

void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty

void initStack(stack* s);
void cleanStack(stack* s);

void printStack(stack* s);

unsigned* resizeUnsignedArray(unsigned* arr, int oldSize, int newSize);

#endif // STACK_H