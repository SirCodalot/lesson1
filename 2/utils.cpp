#include <iostream>
#include "utils.h"
#include "stack.h"


void reverse(int* nums, unsigned size)
{
	int i = 0;

	stack* stk = new stack;
	initStack(stk);

	for (int i = 0; i < size; i++)
	{
		push(stk, nums[i]);
	}

	for (int i = 0; i < size; i++)
	{
		nums[i] = pop(stk);
	}

	delete(stk);
}

int* reverse10()
{
	int i = 0;
	int* nums = new int[10] {};

	for (int i = 0; i < 10; i++)
	{
		nums[i] = -1;
		while (nums[i] < 0)
		{
			std::cout << "Enter a value for #" << i + 1 << ": ";
			std::cin >> nums[i];
		}
	}

	return nums;
}

