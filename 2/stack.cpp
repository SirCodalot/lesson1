#include <iostream>
#include "stack.h"


void initStack(stack* s)
{
	s->_elements = NULL;
	s->_count = 0;
}


void cleanStack(stack* s)
{
	delete[] s->_elements;

	s->_count = 0;
	s->_elements = NULL;
}



void push(stack* s, unsigned element)
{
	s->_count++;
	s->_elements = resizeUnsignedArray(s->_elements, s->_count - 1, s->_count);
	s->_elements[s->_count - 1] = element;
}


int pop(stack* s)
{
	int element = s->_elements[s->_count - 1];
	s->_count--;
	s->_elements = resizeUnsignedArray(s->_elements, s->_count + 1, s->_count);
	return element;
}


unsigned* resizeUnsignedArray(unsigned* arr, int oldSize, int newSize)
{
	int i = 0;
	unsigned* newArr = new unsigned[newSize];

	for (int i = 0; i < oldSize && i < newSize; i++)
	{
		newArr[i] = arr[i];
	}

	delete[] arr;
	return newArr;
}



void printStack(stack* s)
{
	int i = 0;

	for (i = 0; i < s->_count; i++)
	{
		std::cout << i + 1 << ". " << s->_elements[i] << std::endl;
	}
}

