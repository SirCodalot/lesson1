#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct linkedList {
	struct linkedList* next;
	unsigned data;
} linkedList;

void initLinkedList(linkedList* head, unsigned data);
void cleanLinkedList(linkedList* head);

void addToLinkedList(linkedList* head, unsigned num);
linkedList* removeFromLinkedList(linkedList* head, int i);

linkedList* getLastLinkedList(linkedList* head);

void printLinkedList(linkedList* head);

int getLinkedListSize(linkedList* head);

#endif